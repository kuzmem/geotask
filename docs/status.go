package docs

import "gitlab.com/kuzmem/geotask/module/courierfacade/models"

// добавить документацию для роута /api/status

func init() {
	_ = courierGetStatusRequest{}
	_ = courierGetStatusResponse{}
}

// добавить документацию для роута /api/status

// swagger:route GET /api/status courier courierGetStatusRequest
// Статус курьера
// responses:
// 200: courierGetStatusResponse

// swagger:parameters courierGetStatusRequest
type courierGetStatusRequest struct {
}

// swagger:response courierGetStatusResponse
type courierGetStatusResponse struct {
	Body models.CourierStatus
}
