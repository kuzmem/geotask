package run

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kuzmem/geotask/db/cache"
	"gitlab.com/kuzmem/geotask/db/postgres"
	"gitlab.com/kuzmem/geotask/geo"
	cservice "gitlab.com/kuzmem/geotask/module/courier/service"
	cstorage "gitlab.com/kuzmem/geotask/module/courier/storage"
	"gitlab.com/kuzmem/geotask/module/courierfacade/controller"
	cfservice "gitlab.com/kuzmem/geotask/module/courierfacade/service"
	oservice "gitlab.com/kuzmem/geotask/module/order/service"
	ostorage "gitlab.com/kuzmem/geotask/module/order/storage"
	"gitlab.com/kuzmem/geotask/prometeus"
	"gitlab.com/kuzmem/geotask/router"
	"gitlab.com/kuzmem/geotask/server"
	"gitlab.com/kuzmem/geotask/workers/order"

	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта redis
	var (
		orderStorage   ostorage.OrderStorager
		courierStorage cstorage.CourierStorager
	)

	dbType := os.Getenv("DB_TYPE")
	switch dbType {
	case "redis":
		host := os.Getenv("REDIS_HOST")
		port := os.Getenv("REDIS_PORT")

		rclient := cache.NewRedisClient(host, port)

		// инициализация хранилища заказов
		orderStorage = ostorage.NewOrderStorageRedis(&rclient)

		// инициализация хранилища курьеров
		courierStorage = cstorage.NewCourierStorageRedis(rclient)
	case "postgres":
		host := os.Getenv("POSTGRES_HOST")
		port := os.Getenv("POSTGRES_INT_PORT")
		usr := os.Getenv("POSTGRES_USER")
		pwd := os.Getenv("POSTGRES_PWD")
		dbName := os.Getenv("POSTGRES_DB")

		db, err := postgres.NewPostgresDB(host, port, usr, pwd, dbName)
		if err != nil {
			return err
		}

		orderStorage = ostorage.NewOrderStorageSql(db)

		courierStorage = cstorage.NewCourierStorageSql(db)
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	prometeus.RegisterPrometheusMetrics()
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
