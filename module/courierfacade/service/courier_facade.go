package service

import (
	"context"
	"gitlab.com/kuzmem/geotask/module/courier/models"
	cservice "gitlab.com/kuzmem/geotask/module/courier/service"
	cfm "gitlab.com/kuzmem/geotask/module/courierfacade/models"
	models2 "gitlab.com/kuzmem/geotask/module/order/models"
	oservice "gitlab.com/kuzmem/geotask/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
	CourierDeliveredRadius  = 5    // 5m
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Printf("error:%v", err)
		return
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Printf("error:%v", err)
		return
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	var (
		err               error
		stat              cfm.CourierStatus
		courier           *models.Courier
		orders, delivered []models2.Order
		check             bool
	)

	courier, err = c.courierService.GetCourier(ctx)
	if err != nil {
		log.Printf("error:%v", err)
		return stat
	}
	orders, err = c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	if err != nil {
		log.Printf("error:%v", err)
		return stat
	}

	delivered, err = c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierDeliveredRadius, "m")
	if err != nil {
		log.Printf("error:%v", err)
		return stat
	}

	err, check = c.orderService.UpdateDelivered(ctx, delivered)
	if err != nil {
		log.Printf("error:%v", err)
		return stat
	}

	if check {
		err = c.courierService.UpdateCourier(ctx)
		if err != nil {
			log.Printf("error:%v", err)
			return stat
		}
	}

	stat.Courier = *courier
	stat.Orders = orders

	return stat
}
