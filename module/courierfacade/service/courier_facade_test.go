package service

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuzmem/geotask/module/courier/models"
	"gitlab.com/kuzmem/geotask/module/courier/service/mocks"
	cfm "gitlab.com/kuzmem/geotask/module/courierfacade/models"
	omod "gitlab.com/kuzmem/geotask/module/order/models"
	om "gitlab.com/kuzmem/geotask/module/order/service/mocks"
	"testing"
	//"github.com/stretchr/testify/mock"
	//cservice "gitlab.com/kuzmem/geotask/module/courier/service"
	//oservice "gitlab.com/kuzmem/geotask/module/order/service"
)

func TestCourierFacade_MoveCourier(t *testing.T) {
	ctx := context.Background()
	courier := &models.Courier{}
	direction := 1
	zoom := 2

	courierService := new(mocks.Courierer)
	orderService := new(om.Orderer)

	facade := NewCourierFacade(courierService, orderService)

	courierService.On("GetCourier", ctx).Return(courier, nil)
	courierService.On("MoveCourier", *courier, direction, zoom).Return(nil)

	facade.MoveCourier(ctx, direction, zoom)

	courierService.AssertExpectations(t)
}

func TestCourierFacade_GetStatus(t *testing.T) {
	ctx := context.Background()
	courier := &models.Courier{}
	orders := []omod.Order{}

	courierService := new(mocks.Courierer)
	orderService := new(om.Orderer)

	facade := NewCourierFacade(courierService, orderService)

	courierService.On("GetCourier", ctx).Return(courier, nil)
	orderService.On("GetByRadius", ctx, mock.AnythingOfType("float64"), mock.AnythingOfType("float64"), mock.AnythingOfType("float64"), mock.AnythingOfType("string")).Return(orders, nil)
	// Установите ожидаемые значения аргументов для вызова GetByRadius
	courierService.On("Save", ctx, *courier).Return(nil)

	status := facade.GetStatus(ctx)

	courierService.AssertExpectations(t)
	orderService.AssertExpectations(t)

	assert.Equal(t, cfm.CourierStatus{Courier: *courier, Orders: orders}, status)
}
