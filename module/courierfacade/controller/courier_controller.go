package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/kuzmem/geotask/module/courierfacade/service"
	"gitlab.com/kuzmem/geotask/prometeus"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	start := time.Now()

	status := c.courierService.GetStatus(ctx)

	ctx.JSON(http.StatusOK, status)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	elapsed := time.Since(start)
	prometeus.GetStatusDuration.Observe(float64(elapsed.Microseconds()))

}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	var err error
	start := time.Now()
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err = json.Unmarshal(m.Data.([]byte), &cm)
	if err != nil {
		fmt.Println("can't unmarshal data from m.Data")
		return
	}

	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
	elapsed := time.Since(start)
	prometeus.MoveDuration.Observe(float64(elapsed.Microseconds()))

	// вызвать метод MoveCourier у courierService
}
