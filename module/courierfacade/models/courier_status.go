package models

import (
	cm "gitlab.com/kuzmem/geotask/module/courier/models"
	om "gitlab.com/kuzmem/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
