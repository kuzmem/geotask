package storage

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kuzmem/geotask/module/courier/models"
	"gitlab.com/kuzmem/geotask/prometeus"
	"time"
)

type CourierStorageSql struct {
	storage *sqlx.DB
}

func NewCourierStorageSql(storage *sqlx.DB) CourierStorager {
	return &CourierStorageSql{storage: storage}
}

func (c *CourierStorageSql) Save(ctx context.Context, courier models.Courier) error {
	start := time.Now()

	tx, err := c.storage.Beginx()
	if err != nil {
		return fmt.Errorf("save courier err: %v", err)
	}

	stmt := `
			INSERT INTO courier (name, score, location)
			VALUES ($1, $2, st_makepoint($3, $4))
			ON CONFLICT (name) 
			DO UPDATE SET score = EXCLUDED.score, location = EXCLUDED.location;
	`

	if _, err = tx.ExecContext(ctx, stmt, "courier", courier.Score, courier.Location.Lng, courier.Location.Lat); err != nil {
		_ = tx.Rollback()
		return fmt.Errorf("save courier err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "Save").Observe(float64(elapsed.Microseconds()))

	return tx.Commit()
}

func (c *CourierStorageSql) GetOne(ctx context.Context) (*models.Courier, error) {
	var result models.Courier

	start := time.Now()

	stmt := `
			SELECT 
			    score,
			    st_y(st_centroid(st_transform(location, 4326))) AS lat,
			    st_x(st_centroid(st_transform(location, 4326))) AS lng
			FROM courier
			WHERE name = 'courier';
	`

	err := c.storage.QueryRowxContext(ctx, stmt).Scan(&result.Score, &result.Location.Lat, &result.Location.Lng)
	if err != nil && (err.Error() == "pq: operator does not exist: character = courier" || err.Error() == "sql: no rows in result set") {
		return nil, nil
	}
	if err != nil {
		return nil, fmt.Errorf("get courier err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "GetOne").Observe(float64(elapsed.Microseconds()))

	return &result, nil
}
