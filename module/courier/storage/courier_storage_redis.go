package storage

import (
	"context"
	"encoding/json"
	"gitlab.com/kuzmem/geotask/db/cache"
	"gitlab.com/kuzmem/geotask/module/courier/models"
	"gitlab.com/kuzmem/geotask/prometeus"
	"time"
)

//go:generate go run github.com/vektra/mockery/v3 --name=CourierStorager

type CourierStorageRedis struct {
	storage cache.RedisClient
}

func NewCourierStorageRedis(storage cache.RedisClient) CourierStorager {
	return &CourierStorageRedis{storage: storage}
}

func (cs *CourierStorageRedis) Save(ctx context.Context, courier models.Courier) error {
	courierJSON, err := json.Marshal(courier)
	start := time.Now()

	if err != nil {
		return err
	}

	err = cs.storage.Client.Set(ctx, "courier", courierJSON, 0).Err()
	if err != nil {
		return err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "Save").Observe(float64(elapsed.Microseconds()))

	return nil
}

func (cs *CourierStorageRedis) GetOne(ctx context.Context) (*models.Courier, error) {
	start := time.Now()
	val, err := cs.storage.Client.Get(ctx, "courier").Result()
	if err != nil {
		return nil, err
	}

	courier := &models.Courier{}
	err = json.Unmarshal([]byte(val), courier)
	if err != nil {
		return nil, err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("courier", "GetOne").Observe(float64(elapsed.Microseconds()))

	return courier, nil
}
