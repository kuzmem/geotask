package service

import (
	"context"
	"errors"
	"gitlab.com/kuzmem/geotask/module/courier/storage/mocks"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuzmem/geotask/geo"
	"gitlab.com/kuzmem/geotask/module/courier/models"
)

var (
	allowed    = geo.NewAllowedZone()
	disallowed = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestCourierService_GetCourier_Success(t *testing.T) {
	ctx := context.Background()

	// Создаем мок хранилища курьера
	courierStorageMock := mocks.CourierStorager{}

	// Создаем экземпляр сервиса курьера с использованием мока хранилища
	courierService := NewCourierService(&courierStorageMock, allowed, disallowed)

	// Ожидаем, что метод GetOne вызывается один раз и возвращает курьера
	courier := &models.Courier{Location: models.Point{Lat: 59.9311, Lng: 30.3609}}
	courierStorageMock.On("GetOne", ctx).Return(courier, nil)

	// Вызываем метод GetCourier и проверяем результат
	result, err := courierService.GetCourier(ctx)
	assert.NoError(t, err)
	assert.Equal(t, courier, result)

	// Проверяем, что все ожидания мока были выполнены
	courierStorageMock.AssertExpectations(t)
}

func TestCourierService_GetCourier_Error(t *testing.T) {
	ctx := context.Background()

	// Создаем мок хранилища курьера
	courierStorageMock := mocks.CourierStorager{}

	// Создаем экземпляр сервиса курьера с использованием мока хранилища
	courierService := NewCourierService(&courierStorageMock, allowed, disallowed)

	// Ожидаем, что метод GetOne вызывается один раз и возвращает ошибку
	expectedErr := errors.New("storage error")
	courierStorageMock.On("GetOne", ctx).Return(nil, expectedErr)

	// Вызываем метод GetCourier и проверяем результат
	result, err := courierService.GetCourier(ctx)
	assert.Nil(t, result)
	assert.Equal(t, expectedErr, err)

	// Проверяем, что все ожидания мока были выполнены
	courierStorageMock.AssertExpectations(t)
}

func TestCourierService_MoveCourier(t *testing.T) {
	// Создаем курьера для теста
	courier := &models.Courier{Location: models.Point{Lat: 59.9311, Lng: 30.3609}}

	// Создаем мок хранилища курьера
	courierStorageMock := mocks.CourierStorager{}

	// Создаем экземпляр сервиса курьера с использованием мока хранилища
	courierService := NewCourierService(&courierStorageMock, allowed, disallowed)

	// Ожидаем, что метод Save будет вызван один раз с ожидаемыми аргументами
	expectedCourier := models.Courier{
		Score:    0,
		Location: models.Point{Lat: 59.9311, Lng: 30.3769},
	}
	courierStorageMock.On("Save", mock.Anything, expectedCourier).Return(nil)

	// Вызываем метод MoveCourier и проверяем, что он выполняется без ошибок
	err := courierService.MoveCourier(*courier, DirectionRight, 10)
	assert.NoError(t, err)

	// Проверяем, что все ожидания мока были выполнены
	courierStorageMock.AssertExpectations(t)
}

func TestCourierService_MoveCourier_OutOfZone(t *testing.T) {
	// Создаем курьера для теста
	courier := &models.Courier{Location: models.Point{Lat: 59.9311, Lng: 30.3609}}
	// Создаем мок хранилища курьера
	courierStorageMock := mocks.CourierStorager{}

	// Создаем экземпляр сервиса курьера с использованием мока хранилища
	courierService := NewCourierService(&courierStorageMock, allowed, disallowed)

	// Ожидаем, что метод Save вызывается один раз с новыми координатами курьера
	courierStorageMock.On("Save", mock.Anything, mock.AnythingOfType("models.Courier")).Return(nil)

	// Вызываем метод MoveCourier и проверяем, что курьер перемещается внутри зоны
	err := courierService.MoveCourier(*courier, DirectionRight, 10)
	assert.NoError(t, err)

	// Проверяем, что все ожидания мока были выполнены
	courierStorageMock.AssertExpectations(t)
}
