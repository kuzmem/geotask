package service

import (
	"context"
	"gitlab.com/kuzmem/geotask/geo"
	"gitlab.com/kuzmem/geotask/module/courier/models"
	"gitlab.com/kuzmem/geotask/module/courier/storage"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
	UpdateCourier(ctx context.Context) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) UpdateCourier(ctx context.Context) error {
	var (
		err     error
		courier *models.Courier
	)

	courier, err = c.courierStorage.GetOne(ctx)
	if err != nil {
		return err
	}

	courier.Score += 1

	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		return err
	}

	return nil
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go

	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	// сохраняем новые координаты курьера
	var (
		err     error
		courier *models.Courier
	)

	courier, err = c.courierStorage.GetOne(ctx)
	if err != nil {
		return nil, err
	}
	if courier == nil {
		courier = &models.Courier{
			Score: 0,
			Location: models.Point{
				Lat: DefaultCourierLat,
				Lng: DefaultCourierLng,
			},
		}
	}

	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
	}

	randPoint := models.Point{
		Lat: point.Lat,
		Lng: point.Lng,
	}

	courier.Location = randPoint
	err = c.courierStorage.Save(ctx, *courier)
	if err != nil {
		return nil, err
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты

	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны

	// далее сохранить изменения в хранилище

	switch direction {
	case DirectionUp:
		courier.Location.Lat += 0.001 / math.Pow(2, float64(zoom-14))
	case DirectionDown:
		courier.Location.Lat -= 0.001 / math.Pow(2, float64(zoom-14))
	case DirectionLeft:
		courier.Location.Lng -= 0.001 / math.Pow(2, float64(zoom-14))
	case DirectionRight:
		courier.Location.Lng += 0.001 / math.Pow(2, float64(zoom-14))
	}

	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
	}

	randPoint := models.Point{
		Lat: point.Lat,
		Lng: point.Lng,
	}

	courier.Location = randPoint

	ctx := context.Background()

	err := c.courierStorage.Save(ctx, courier)
	if err != nil {
		return err
	}

	return nil
}
