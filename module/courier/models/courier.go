package models

type Courier struct {
	Score    int   `json:"score" db:"score"`
	Location Point `json:"location" db:"location"`
}

type Point struct {
	Lat float64 `json:"lat" db:"lat"`
	Lng float64 `json:"lng" db:"lng"`
}
