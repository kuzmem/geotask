package service

import (
	"context"
	"gitlab.com/kuzmem/geotask/geo"
	"gitlab.com/kuzmem/geotask/module/order/models"
	"gitlab.com/kuzmem/geotask/module/order/storage"
	"math/rand"
	"time"
)

//go:generate go run github.com/vektra/mockery/v3 --name=Orderer

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // возвращает заказы через метод storage.GetByRadius
	Save(ctx context.Context, order models.Order) error                                             // сохраняет заказ через метод storage.Save с заданным временем жизни OrderMaxAge
	GetCount(ctx context.Context) (int, error)                                                      // возвращает количество заказов через метод storage.GetCount
	RemoveOldOrders(ctx context.Context) error                                                      // удаляет старые заказы через метод storage.RemoveOldOrders с заданным временем жизни OrderMaxAge
	GenerateOrder(ctx context.Context) error                                                        // генерирует заказ в случайной точке из разрешенной зоны, с уникальным id, ценой и ценой доставки
	UpdateDelivered(ctx context.Context, orders []models.Order) (error, bool)
}

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (o *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	return o.storage.GetByRadius(ctx, lng, lat, radius, unit)
}

func (o *OrderService) Save(ctx context.Context, order models.Order) error {
	return o.storage.Save(ctx, order, orderMaxAge+3)
}

func (o *OrderService) GetCount(ctx context.Context) (int, error) {
	return o.storage.GetCount(ctx)
}

func (o *OrderService) RemoveOldOrders(ctx context.Context) error {
	return o.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (o *OrderService) GenerateOrder(ctx context.Context) error {
	var (
		order models.Order
		err   error
	)

	point := geo.GetRandomAllowedLocation(o.allowedZone, o.disabledZones)
	order.Lat = point.Lat
	order.Lng = point.Lng
	order.DeliveryPrice = rand.Float64()*(maxDeliveryPrice-minDeliveryPrice) + minDeliveryPrice
	order.Price = rand.Float64()*(maxOrderPrice-minOrderPrice) + minOrderPrice
	order.IsDelivered = false
	order.CreatedAt = time.Now()

	err = o.Save(ctx, order)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderService) UpdateDelivered(ctx context.Context, orders []models.Order) (error, bool) {
	return o.storage.UpdateDelivered(ctx, orders)
}
