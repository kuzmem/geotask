package service

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuzmem/geotask/geo"
	"gitlab.com/kuzmem/geotask/module/order/models"
	"gitlab.com/kuzmem/geotask/module/order/storage/mocks"
)

var (
	allowed    = geo.NewAllowedZone()
	disallowed = []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}
)

func TestOrderService_GetByRadius(t *testing.T) {
	ctx := context.Background()

	expectedOrders := []models.Order{
		{
			ID:            1,
			Price:         1500.00,
			DeliveryPrice: 200.00,
			Lng:           30.3609,
			Lat:           59.9471,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
		{
			ID:            2,
			Price:         2500.00,
			DeliveryPrice: 300.00,
			Lng:           30.361,
			Lat:           59.948,
			IsDelivered:   false,
			CreatedAt:     time.Now(),
		},
	}

	mockStorage := &mocks.OrderStorager{}
	mockStorage.On("GetByRadius", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(expectedOrders, nil)

	orderService := NewOrderService(mockStorage, allowed, disallowed)
	orders, err := orderService.GetByRadius(ctx, 30.3609, 59.9471, 1000.0, "m")
	assert.NoError(t, err)
	assert.Equal(t, expectedOrders, orders)

	mockStorage.AssertExpectations(t)
}

func TestOrderService_Save(t *testing.T) {
	ctx := context.Background()

	order := models.Order{
		ID:            1,
		Price:         1500.00,
		DeliveryPrice: 200.00,
		Lng:           30.3609,
		Lat:           59.9471,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	}

	mockStorage := &mocks.OrderStorager{}
	mockStorage.On("Save", mock.Anything, mock.Anything, mock.Anything).Return(nil)

	orderService := NewOrderService(mockStorage, allowed, disallowed)
	err := orderService.Save(ctx, order)
	assert.NoError(t, err)

	mockStorage.AssertExpectations(t)
}

func TestOrderService_GetCount(t *testing.T) {
	ctx := context.Background()

	mockStorage := &mocks.OrderStorager{}
	mockStorage.On("GetCount", mock.Anything).Return(10, nil)

	orderService := NewOrderService(mockStorage, allowed, disallowed)
	count, err := orderService.GetCount(ctx)
	assert.NoError(t, err)
	assert.Equal(t, 10, count)

	mockStorage.AssertExpectations(t)
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	mockStorage := &mocks.OrderStorager{}
	mockStorage.On("RemoveOldOrders", context.Background(), mock.AnythingOfType("time.Duration")).Return(nil).Once()

	orderService := NewOrderService(mockStorage, allowed, disallowed)
	err := orderService.RemoveOldOrders(context.Background())
	assert.NoError(t, err)

	mockStorage.AssertExpectations(t)
}

func TestOrderService_GenerateOrder(t *testing.T) {
	ctx := context.Background()

	mockStorage := &mocks.OrderStorager{}
	mockStorage.On("GenerateUniqueID", ctx).Return(int64(1), nil)
	mockStorage.On("Save", ctx, mock.Anything, mock.Anything).Return(nil)

	orderService := NewOrderService(mockStorage, allowed, disallowed)
	err := orderService.GenerateOrder(ctx)
	assert.NoError(t, err)

	mockStorage.AssertExpectations(t)
}
