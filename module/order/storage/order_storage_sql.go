package storage

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/kuzmem/geotask/module/order/models"
	"gitlab.com/kuzmem/geotask/prometeus"
	"time"
)

type OrderStorageSql struct {
	storage *sqlx.DB
}

func NewOrderStorageSql(storage *sqlx.DB) OrderStorager {
	return &OrderStorageSql{storage: storage}
}

func (o *OrderStorageSql) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	start := time.Now()

	tx, err := o.storage.Beginx()
	if err != nil {
		return fmt.Errorf("save order err: %v", err)
	}

	stmt := `
			INSERT INTO orders (price, delivery_price, lng, lat, location, is_delivered, created_at) 
			VALUES ($1, $2, $3, $4, st_makepoint($3, $4), $5, $6)
			`

	if _, err = tx.ExecContext(ctx, stmt, order.Price, order.DeliveryPrice, order.Lng, order.Lat, order.IsDelivered, order.CreatedAt); err != nil {
		_ = tx.Rollback()
		return fmt.Errorf("save order err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "Save").Observe(float64(elapsed.Microseconds()))

	return tx.Commit()
}

func (o *OrderStorageSql) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	//TODO implement me
	panic("implement me")
}

func (o *OrderStorageSql) GenerateUniqueID(ctx context.Context) (int64, error) {
	//TODO implement me
	panic("implement me")
}

func (o *OrderStorageSql) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var (
		out []models.Order
		err error
	)

	start := time.Now()

	stmt := fmt.Sprintf(`
			SELECT order_id, price, delivery_price, lng, lat, is_delivered, created_at
			FROM orders
			WHERE st_distancesphere(location, st_makepoint(%f, %f)) <= %f;
	`, lng, lat, radius)

	err = o.storage.SelectContext(ctx, &out, stmt)
	if err != nil {
		return nil, fmt.Errorf("get by radius err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "GetByRadius").Observe(float64(elapsed.Microseconds()))

	return out, nil
}

func (o *OrderStorageSql) GetCount(ctx context.Context) (int, error) {
	var count int

	start := time.Now()

	if err := o.storage.QueryRowxContext(ctx, `SELECT count(*) FROM orders;`).Scan(&count); err != nil {
		return 0, fmt.Errorf("get count err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "GetCount").Observe(float64(elapsed.Microseconds()))

	return count, nil
}

func (o *OrderStorageSql) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	start := time.Now()

	stmtAge := time.Now().Add(-maxAge)

	tx, err := o.storage.Beginx()
	if err != nil {
		return fmt.Errorf("remove orders err: %v", err)
	}

	_, err = tx.ExecContext(ctx, fmt.Sprintf("DELETE FROM orders WHERE created_at <= '%v'", stmtAge.Format("2006-01-02 15:04:05")))
	if err != nil {
		_ = tx.Rollback()
		return fmt.Errorf("remove orders err: %v", err)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "RemoveOldOrders").Observe(float64(elapsed.Microseconds()))

	return tx.Commit()
}

func (o *OrderStorageSql) UpdateDelivered(ctx context.Context, orders []models.Order) (error, bool) {
	start := time.Now()

	if len(orders) == 0 {
		return nil, false
	}

	tx, err := o.storage.Beginx()
	if err != nil {
		_ = tx.Rollback()
		return fmt.Errorf("update err: %v", err), false
	}
	stmt := fmt.Sprintf("DELETE FROM orders WHERE order_id = '%v'", orders[0].ID)

	_, err = tx.ExecContext(ctx, stmt)
	if err != nil {
		_ = tx.Rollback()
		return fmt.Errorf("update delivered err: %v", err), false
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "UpdateDelivered").Observe(float64(elapsed.Microseconds()))

	return tx.Commit(), true
}
