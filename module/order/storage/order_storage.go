package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/kuzmem/geotask/db/cache"
	"gitlab.com/kuzmem/geotask/prometeus"

	"github.com/redis/go-redis/v9"
	"gitlab.com/kuzmem/geotask/module/order/models"

	"time"
)

//go:generate go run github.com/vektra/mockery/v3 --name=OrderStorager

type OrderStorageRedis struct {
	storage *cache.RedisClient
}

func NewOrderStorageRedis(storage *cache.RedisClient) OrderStorager {
	return &OrderStorageRedis{storage: storage}
}

func (o *OrderStorageRedis) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	start := time.Now()

	var err error
	order.ID, err = o.GenerateUniqueID(ctx)
	if err != nil {
		return err
	}

	err = o.saveOrderWithGeo(ctx, order, maxAge)

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "Save").Observe(float64(elapsed.Microseconds()))
	return err
}

func (o *OrderStorageRedis) UpdateDelivered(ctx context.Context, orders []models.Order) (error, bool) {
	var (
		err error
	)
	start := time.Now()

	if len(orders) == 0 {
		return nil, false
	}

	for i := range orders {
		orders[i].IsDelivered = true
		order := fmt.Sprintf("order:%v", orders[i].ID)
		err = o.storage.Client.ZRem(ctx, "geoindex", order).Err()
		if err != nil {
			return err, false
		}
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "UpdateDelivered").Observe(float64(elapsed.Microseconds()))
	return nil, true
}

func (o *OrderStorageRedis) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	var err error
	var old *redis.ZRangeBy
	var orders []string
	// получить ID всех старых ордеров, которые нужно удалить
	// используя метод ZRangeByScore
	// старые ордеры это те, которые были созданы две минуты назад
	// и более
	/**
	&redis.ZRangeBy{
		Max: использовать вычисление времени с помощью maxAge,
		Min: "0",
	}
	*/

	// Проверить количество старых ордеров
	// удалить старые ордеры из redis используя метод ZRemRangeByScore где ключ "orders" min "-inf" max "(время создания старого ордера)"
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни
	start := time.Now()

	mxAge := fmt.Sprintf("%v", float64(time.Now().Unix())-maxAge.Seconds()+1)
	old = &redis.ZRangeBy{
		Min:    "0",
		Max:    mxAge,
		Offset: 0,
		Count:  0,
	}
	orders, err = o.storage.Client.ZRangeByScore(ctx, "orders", old).Result()
	if err != nil {
		return err
	}

	for _, item := range orders {
		err = o.storage.Client.ZRem(ctx, "geoindex", item).Err()
		if err != nil {
			return err
		}
	}

	err = o.storage.Client.ZRemRangeByScore(ctx, "orders", "-inf", mxAge).Err()
	if err != nil {
		return err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "RemoveOldOrders").Observe(float64(elapsed.Microseconds()))
	return nil
}

func (o *OrderStorageRedis) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var data string
	var order models.Order
	// получаем ордер из redis по ключу order:ID

	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil

	// десериализуем ордер из json
	start := time.Now()

	query := fmt.Sprintf("order:%v", orderID)
	data, err = o.storage.Client.Get(ctx, query).Result()
	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	err = json.Unmarshal([]byte(data), &order)
	if err != nil {
		return nil, err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "GetByID").Observe(float64(elapsed.Microseconds()))
	return &order, nil
}

func (o *OrderStorageRedis) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)
	if err != nil {
		return err
	}

	// сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge
	key := fmt.Sprintf("order:%v", order.ID)
	err = o.storage.Client.Set(ctx, key, data, maxAge).Err()
	if err != nil {
		return err
	}

	// добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты

	location := &redis.GeoLocation{
		Name:      key,
		Longitude: order.Lng,
		Latitude:  order.Lat,
	}
	err = o.storage.Client.GeoAdd(ctx, "geoindex", location).Err()
	if err != nil {
		return err
	}

	// zset сохраняем ордер для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.Client.ZAdd(ctx, "order_scores", redis.Z{
		Score:  float64(order.CreatedAt.Unix()),
		Member: key,
	}).Err()
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorageRedis) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	start := time.Now()

	count, err := o.storage.Client.ZCard(ctx, "orders").Result()
	if err != nil {
		return 0, err
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "GetCount").Observe(float64(elapsed.Microseconds()))
	return int(count), nil
}

func (o *OrderStorageRedis) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var orders []models.Order
	var data string
	var ordersLocation []redis.GeoLocation
	start := time.Now()

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	orders = make([]models.Order, 0, len(ordersLocation))
	// проходим по списку ID заказов и получаем данные о заказе
	// получаем данные о заказе по ID из redis по ключу order:ID
	for i := range ordersLocation {
		var order models.Order
		data, err = o.storage.Client.Get(ctx, ordersLocation[i].Name).Result()
		if err == redis.Nil {
			return nil, nil
		} else if err != nil {
			return nil, err
		}
		err = json.Unmarshal([]byte(data), &order)
		if err != nil {
			return nil, err
		}

		orders = append(orders, order)
	}

	elapsed := time.Since(start)
	prometeus.RepositoryMethodsDuration.WithLabelValues("orders", "GetByRadius").Observe(float64(elapsed.Microseconds()))

	return orders, nil
}

func (o *OrderStorageRedis) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	var err error
	var geos []redis.GeoLocation
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки
	/**
	&redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	*/

	geoRadusQuery := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}

	geos, err = o.storage.Client.GeoRadius(ctx, "geoindex", lng, lat, geoRadusQuery).Result()
	if err == redis.Nil {
		return nil, redis.Nil
	} else if err != nil {
		return nil, err
	}
	return geos, nil
}

func (o *OrderStorageRedis) GenerateUniqueID(ctx context.Context) (int64, error) {
	var err error
	var id int64

	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	order := fmt.Sprintf("order:%v", id)
	id, err = o.storage.Client.Incr(ctx, order).Result()
	if err != nil {
		return 0, err
	}

	return id, nil
}
