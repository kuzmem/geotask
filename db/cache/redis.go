package cache

import (
	"context"
	"github.com/redis/go-redis/v9"
)

type RedisClient struct {
	Client *redis.Client
}

func NewRedisClient(host, port string) RedisClient {
	// реализуйте создание клиента для Redis
	client := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})
	rClient := RedisClient{Client: client}
	return rClient

}

func (rc *RedisClient) Ping(ctx context.Context) *redis.StatusCmd {
	status := rc.Client.Ping(ctx)
	return status
}
